const button = document.querySelector("#kirim");

button.addEventListener("click", () => {
  const dataFormulir = new FormData(document.getElementById("formulir"));

  axios.post("tugas3_225150407111085.php", dataFormulir).then((response) => {
    if (validasiForm()) {
      alert("Mohon isi semua formulir!");
      return;
    }
    document.getElementById("hasil").innerHTML = response.data;
  });
});

function validasiForm() {
  const nama = document.getElementById("nama").value;
  const nim = document.getElementById("nim").value;
  const alamat = document.getElementById("alamat").value;
  const telp = document.getElementById("telp").value;
  const jenisKelamin = document.querySelector(
    'input[name="jenisKelamin"]:checked'
  );
  const tanggalLahir = document.getElementById("tanggalLahir").value;
  const programStudi = document.getElementById("programStudi").value;

  if (
    nama == "" ||
    nim === "" ||
    alamat === "" ||
    telp === "" ||
    !jenisKelamin ||
    tanggalLahir === "" ||
    programStudi === ""
  ) {
    return true;
  } else {
    return false;
  }
}
